<?php
if ( ! class_exists( 'VFH_User_Controls' ) ) {
	class VFH_User_Controls {
		public $v = '0.3.2';
		public $options_page_slug = 'vfh_login';
		public $options = array();
		public $defaults = array();
		
		/**
		 * Instantiate our object
		 */
		function __construct() {
			/**
			 * Set up our standard/default options
			 */
			$this->do_defaults();
			
			/**
			 * Hook into some standard places to start with
			 */
			add_action( 'init', array( $this, 'init' ) );
			add_action( 'admin_init', array( $this, 'admin_init' ) );
			add_action( 'template_redirect', array( $this, 'template_redirect' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
			add_action( 'admin_menu', array( $this, 'admin_menu' ) );
			
			/**
			 * Add some filters and actions to change behavior
			 */
			add_action( 'lost_password', array( $this, 'set_network_site_url' ), 99 );
			add_action( 'lostpassword_form', array( $this, 'reset_network_site_url' ), 1 );
			add_action( 'validate_password_reset', array( $this, 'set_network_site_url' ), 99 );
			add_action( 'resetpass_form', array( $this, 'reset_network_site_url' ), 1 );
			add_filter( 'login_url', array( $this, 'login_url' ), 99 );
			add_filter( 'register_url', array( $this, 'registration_url' ), 99 );
			add_filter( 'lostpassword_url', array( $this, 'lostpassword_url' ), 99 );
			add_action( 'wp_logout', array( $this, 'redirect_after_logout' ) );
			add_filter( 'retrieve_password_message', array( $this, 'retrieve_password_message' ), 99, 4 );
			add_filter( 'retrieve_password_title', array( $this, 'retrieve_password_title' ), 99 );
			add_filter( 'login_redirect', array( $this, 'login_redirect' ), 1, 3 );
		}
		
		/**
		 * Set up some default settings/options
		 * @uses VFH_User_Controls::$user_levels to set that up
		 * @uses apply_filters() to allow filtering of the user levels with the vfh-login-user-levels filter
		 * @uses VFH_User_Controls::$options to set up those values
		 * @uses apply_filters() to allow filtering the default options with the vfh-login-defaults filter
		 */
		function do_defaults() {
			$this->user_levels = apply_filters( 'vfh-login-user-levels', array(
				'subscriber'    => array(
					'name'      => __( 'Subscriber', 'vfhlogin' ), 
					'captotest' => 'edit_posts',
				), 
				'contributor'   => array( 
					'name'      => __( 'Contributor', 'vfhlogin' ), 
					'captotest' => 'publish_posts', 
				), 
				'author'        => array( 
					'name'      => __( 'Author', 'vfhlogin' ), 
					'captotest' => 'edit_pages', 
				), 
				'editor'        => array(
					'name'      => __( 'Editor', 'vfhlogin' ), 
					'captotest' => 'promote_users', 
				), 
				'administrator' => array(
					'name'      => __( 'Administrator', 'vfhlogin' ), 
					'captotest' => 'delete_users', 
				)
			) );
			
			$this->defaults = apply_filters( 'vfh-login-defaults', array( 
				'lost_password_subject' => null, 
				'lost_password_message' => null, 
				'hide_admin_bar'        => false,
				'hide_admin_bar_from'   => null, 
				'hide_backend'          => false, 
				'hide_backend_from'     => null, 
				'login_url_id'          => null, 
				'register_url_id'       => null, 
				'logout_redirect'       => false, 
				'logout_redirect_to'    => null, 
				'login_redirect_to'     => null, 
			) );
			
			$this->get_settings();
		}
		
		/**
		 * Retrieve the settings for this plugin from the database
		 */
		function get_settings() {
			$options = get_option( 'vfh-login-options', array() );
			$this->options = array_merge( $this->defaults, $options );
		}
		
		/**
		 * Perform any actions that need to occur on the init hook
		 * @uses add_shortcode() to add the login_form shortcode
		 * @see VFH_User_Controls::login_shortcode()
		 * @uses add_shortcode() to add the logged_in shortcode
		 * @see VFH_User_Controls::logged_in_shortcode()
		 * @uses add_shortcode() to add the logged_out shortcode
		 * @see VFH_User_Controls::logged_out_shortcode()
		 */
		function init() {
			add_shortcode( 'login_form', array( $this, 'login_shortcode' ) );
			add_shortcode( 'logged_in', array( $this, 'logged_in_shortcode' ) );
			add_shortcode( 'logged_out', array( $this, 'logged_out_shortcode' ) );
			
			if ( ! is_user_logged_in() )
				return;
			
			if ( true === $this->options['hide_admin_bar'] && ! empty( $this->options['hide_admin_bar_from'] ) ) {
				$level = $this->options['hide_admin_bar_from'];
				
				if ( ! current_user_can( $this->user_levels[$level]['captotest'] ) ) {
					add_filter( 'show_admin_bar', '__return_false' );
				}
			}
		}
		
		/**
		 * Perform any actions that need to occur on the admin_init hook
		 * @uses register_setting() to whitelist our plugin options
		 * @uses add_settings_section() to register the section where our options will appear
		 * @uses add_settings_field() to register the individual option form fields
		 */
		function admin_init() {
			if ( is_admin() ) {
				if ( true === $this->options['hide_backend'] && ! empty( $this->options['hide_backend_from'] ) ) {
					$this->hide_backend_from_user( $this->options['hide_backend_from'] );
				}
			}
			
			register_setting( 
				/* $option_group = */ $this->options_page_slug, 
				/* $option_name  = */ 'vfh-login-options', 
				/* $callback     = */ array( $this, 'sanitize_options' )
			);
			
			add_settings_section(
				/* $id       = */ 'vfh_login_password_section', 
				/* $title    = */ __( 'Password Retrieval Options', 'vfhlogin' ), 
				/* $callback = */ array( $this, 'do_settings_section' ), 
				/* $page     = */ $this->options_page_slug
			);
			
			add_settings_section(
				/* $id       = */ 'vfh_login_hiding_section', 
				/* $title    = */ __( 'Backend Visibility Options', 'vfhlogin' ), 
				/* $callback = */ array( $this, 'do_settings_section' ), 
				/* $page     = */ $this->options_page_slug
			);
			
			add_settings_section(
				/* $id       = */ 'vfh_login_redirect_section', 
				/* $title    = */ __( 'Redirection Options', 'vfhlogin' ), 
				/* $callback = */ array( $this, 'do_settings_section' ), 
				/* $page     = */ $this->options_page_slug
			);
			
			/**
			 * Set up an array with our field IDs/names as the keys and our labels as the values
			 */
			$fields = apply_filters( 'vfh-login-options-fields', array(
				'lost_password_subject' => __( 'Custom Lost Password Subject', 'vfhlogin' ), 
				'lost_password_message' => __( 'Custom Lost Password Message', 'vfhlogin' ), 
				'hide_admin_bar'        => __( 'Hide the admin bar from specific user groups?', 'vfhlogin' ),
				'hide_admin_bar_from'   => __( 'If so, hide it from all users at or below which level?', 'vfhlogin' ), 
				'hide_backend'          => __( 'Hide the admin backend from specific user groups?', 'vfhlogin' ), 
				'hide_backend_from'     => __( 'If so, hide it from all users at or below which level?', 'vfhlogin' ), 
				'login_redirect_to'     => __( 'If the backend is hidden from a user, to which page should they be redirected to after logging in?', 'vfhlogin' ), 
				'login_url_id'          => __( 'Which page, if any, should serve as the custom Login page?', 'vfhlogin' ), 
				'register_url_id'       => __( 'Which page, if any, should be used as the custom Registration page?', 'vfhlogin' ), 
				'logout_redirect'       => __( 'Should users be redirected to a specific frontend page when they logout?', 'vfhlogin' ), 
				'logout_redirect_to'    => __( 'If so, to which page should they be redirected?', 'vfhlogin' ), 
			) );
			
			/**
			 * Register each of our individual settings fields
			 */
			foreach ( $fields as $k => $l ) {
				
				switch ( $k ) {
					case 'lost_password_message' : 
					case 'lost_password_subject' : 
						$group = 'vfh_login_password_section';
					break;
					case 'hide_admin_bar' : 
					case 'hide_admin_bar_from' : 
					case 'hide_backend' : 
					case 'hide_backend_from' : 
						$group = 'vfh_login_hiding_section';
					break;
					default : 
						$group = 'vfh_login_redirect_section';
					break;
				}
				
				add_settings_field(
					/* $id       = */ sprintf( 'vfh-login-%s', $k ), 
					/* $title    = */ $l, 
					/* $callback = */ array( $this, 'do_settings_field' ), 
					/* $page     = */ $this->options_page_slug, 
					/* $section  = */ $group, 
					/* $args     = */ array( 'label_for' => sprintf( 'vfh-login-%s', $k ), 'name' => $k )
				);
				
			}
		}
		
		/**
		 * Manipulate the admin menu as necessary
		 * @uses add_users_page() to add a new admin page to the Users menu
		 */
		function admin_menu() {
			add_users_page( 
				__( 'Custom Login Settings', 'vfhlogin' ), 
				__( 'Custom Login Settings', 'vfhlogin' ), 
				'delete_users', 
				$this->options_page_slug, 
				array( $this, 'options_page' )
			);
		}
		
		/**
		 * Set up the options page
		 */
		function options_page() {
?>
<div class="wrap">
	<h2>Custom Login Settings</h2>
	<form method="post" action="options.php" novalidate>
		<?php settings_fields( $this->options_page_slug ) ?>
		<?php do_settings_sections( $this->options_page_slug ) ?>
		<p><input type="submit" value="<?php _e( 'Save', 'vfhlogin' ) ?>" class="button button-primary"/></p>
	</form>
</div>
<?php
		}
		
		/**
		 * Sanitize the plugin settings before saving them to the database
		 */
		function sanitize_options( $input=array() ) {
			if ( ! wp_verify_nonce( $_POST['_wpnonce'], $this->options_page_slug . '-options' ) ) {
				return false;
			}
			
			/*print( '<pre><code>' );
			var_dump( $_POST );
			print( '</code></pre>' );
			wp_die( 'Done' );*/
			
			$output = array();
			foreach ( $input as $k => $v ) {
				switch( $k ) {
					case 'lost_password_message' : 
						$output[$k] = esc_textarea( $v );
					break;
					case 'hide_admin_bar' : 
					case 'hide_backend' : 
					case 'logout_redirect' : 
						if ( isset( $input[$k] ) && '1' == $input[$k] ) {
							$output[$k] = true;
						} else {
							$output[$k] = false;
						}
					break;
					case 'hide_admin_bar_from' : 
					case 'hide_backend_from' : 
						if ( array_key_exists( $v, $this->user_levels ) ) {
							$output[$k] = $v;
						} else {
							$output[$k] = null;
						}
					break;
					case 'login_url_id' : 
					case 'register_url_id' : 
					case 'logout_redirect_to' : 
					case 'login_redirect_to' : 
						$tmp = absint( $v );
						$t = get_post( $tmp );
						if ( is_object( $t ) && ! is_wp_error( $t ) && $t->ID == $tmp ) {
							$output[$k] = $tmp;
						} else {
							$output[$k] = 0;
						}
					break;
					default : 
						$output[$k] = esc_attr( $v );
					break;
				}
			}
			
			return array_merge( $this->defaults, $output );
		}
		
		/**
		 * Outputs any information we need in our settings section
		 */
		function do_settings_section() {
			return;
		}
		
		/**
		 * Output an individual settings field
		 * @uses do_action() to allow handling other settings fields - when using this, make sure to check the $args array before doing anything
		 */
		function do_settings_field( $args=array() ) {
			if ( ! array_key_exists( 'label_for', $args ) || ! array_key_exists( 'name', $args ) )
				return false;
			
			switch ( $args['name'] ) {
				case 'lost_password_message' : 
					/**
					 * Output a textarea for the lost password message field
					 */
					wp_editor( html_entity_decode( stripslashes( $this->options[$args['name']] ) ), $args['label_for'], array( 'media_buttons' => false, 'textarea_name' => sprintf( 'vfh-login-options[%s]', $args['name'] ), 'teeny' => true ) );
				break;
				case 'hide_admin_bar' : 
				case 'hide_backend' : 
				case 'logout_redirect' : 
					/**
					 * Output a checkbox for the boolean fields
					 */
					printf( '<input type="checkbox" value="1" name="vfh-login-options[%1$s]" id="%2$s"%3$s/>', $args['name'], $args['label_for'], checked( $this->options[$args['name']], true, false ) );
				break;
				case 'hide_admin_bar_from' : 
				case 'hide_backend_from' : 
					/**
					 * Output a select list with user levels
					 */
					$opts = '';
					foreach ( $this->user_levels as $n => $v ) {
						$opts .= sprintf( '<option value="%1$s"%3$s>%2$s</option>', $n, $v['name'], selected( $this->options[$args['name']], $n, false ) );
					}
					printf( '<select name="vfh-login-options[%1$s]" id="%2$s" class="widefat">%3$s</select>', $args['name'], $args['label_for'], $opts );
				break;
				case 'login_url_id' : 
				case 'register_url_id' : 
					/**
					 * Output a select list of published pages
					 */
					wp_dropdown_pages( array( 
						'selected' => $this->options[$args['name']], 
						'name'     => sprintf( 'vfh-login-options[%s]', $args['name'] ), 
						'id'       => $args['label_for'], 
						'show_option_none' => __( '-- Do not redirect --', 'vfhlogin' ), 
						'option_none_value' => 0
					) );
				break;
				case 'logout_redirect_to' : 
				case 'login_redirect_to' : 
					/**
					 * Output a select list of published pages
					 */
					wp_dropdown_pages( array( 
						'selected' => $this->options[$args['name']], 
						'name'     => sprintf( 'vfh-login-options[%s]', $args['name'] ), 
						'id'       => $args['label_for'], 
						'show_option_none' => __( '-- Use the front page --', 'vfhlogin' ), 
						'option_none_value' => 0
					) );
				break;
				default : 
					/**
					 * Output a text input
					 */
					printf( '<input class="widefat" type="text" name="vfh-login-options[%1$s]" id="%2$s" value="%3$s"/>', $args['name'], $args['label_for'], esc_attr( $this->options[$args['name']] ) );
				break;
			}
			
			do_action( 'vfh-login-do-extra-settings-fields', $args );
		}
		
		/**
		 * Perform any actions that need to occur just before the theme is rendered
		 * Not currently used for anything; just a placeholder in case we need it later
		 */
		function template_redirect() {
			return;
		}
		
		/**
		 * Set up any style sheets that are needed for this plugin
		 * Not currently used for anything; just a placeholder in case we need it later
		 */
		function enqueue_styles() {
			return;
		}
		
		/**
		 * Set up any javascript that needs to be used by this plugin
		 * Not currently used for anything; just a placeholder in case we need it later
		 */
		function enqueue_scripts() {
			return;
		}
		
		/**
		 * Filter the network site URL to be the normal site URL instead
		 */
		function set_network_site_url() {
			add_filter( 'network_site_url', array( $this, 'site_url' ), 99, 3 );
		}
		
		/**
		 * Remove the network site URL filter
		 */
		function reset_network_site_url() {
			remove_filter( 'network_site_url', array( $this, 'site_url' ), 99, 3 );
		}
		
		/**
		 * Return the site URL instead of the network site URL
		 */
		function site_url( $url, $path='', $scheme=null ) {
			return site_url( $path, $scheme );
		}
		
		/**
		 * Filter the registration URL
		 * @uses VFH_User_Controls::$options['register_url_id']
		 * @uses get_permalink() to retrieve the URL to the registration page
		 */
		function registration_url( $url ) {
			if ( ! empty( $this->options['register_url_id'] ) && absint( $this->options['register_url_id'] ) ) {
				$pid = absint( $this->options['register_url_id'] );
				$p = get_permalink( $pid );
				if ( ! empty( $p ) && ! is_wp_error( $p ) ) {
					return $p;
				}
			}
			
			return $url;
		}
		
		/**
		 * Filter the login URL
		 */
		function login_url( $url ) {
			if ( ! empty( $this->options['login_url_id'] ) && absint( $this->options['login_url_id'] ) ) {
				$pid = absint( $this->options['login_url_id'] );
				$p = get_permalink( $pid );
				if ( ! empty( $p ) && ! is_wp_error( $p ) ) {
					return $p;
				}
			}
			
			return $url;
		}
		
		/**
		 * Filter the lost password URL
		 */
		function lostpassword_url( $url ) {
			return str_ireplace( network_site_url( '/' ), site_url( '/' ), $url );
		}
		
		/**
		 * Handles sending the password reset email message to users
		 * @see retrieve_password()
		 */
		function retrieve_password_form() {
			return;
		}
		
		/**
		 * Filter the password retrieval message text
		 */
		function retrieve_password_message( $message, $key, $user_login, $user_data ) {
			if ( ! function_exists( 'is_multisite' ) || ! is_multisite() )
				return $message;
			
			if ( ! empty( $this->options['lost_password_message'] ) ) {
				$message = $this->options['lost_password_message'];
				$message = str_ireplace( array( 
					'{username}', 
					'{reset_url}', 
					'{site_name}', 
					'{site_url}' 
				), array( 
					$user_login, 
					site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ), 
					wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES ), 
					home_url() 
				), $message );
				return do_shortcode( wp_specialchars_decode( $message, ENT_QUOTES ) );
			}
			
			$message = str_ireplace( array( 
				network_site_url( 'wp-login.php' ), 
				network_home_url(), 
				$GLOBALS['current_site']->site_name 
			), array( 
				site_url( 'wp-login.php' ), 
				home_url(), 
				wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES )
			), $message );
			
			return $message;
		}
		
		/**
		 * Filter the subject of the password retrieval message
		 */
		function retrieve_password_title( $title ) {
			if ( ! function_exists( 'is_multisite' ) || ! is_multisite() )
				return $title;
			
			if ( ! empty( $this->options['lost_password_subject'] ) ) {
				$title = str_ireplace( array( 
					'{site_name}', 
					'{site_url}' 
				), array( 
					wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES ), 
					home_url() 
				), $this->options['lost_password_subject'] );
				
				return $title;
			}
			
			$title = str_ireplace( $GLOBALS['current_site']->site_name, wp_specialchars_decode(get_option('blogname'), ENT_QUOTES), $title );
			return $title;
		}
		
		/**
		 * Handle the login form for this site
		 */
		function login() {
			return;
		}
		
		/**
		 * Redirect a user after they log out of the site
		 * @uses VFH_User_Controls::$options['logout_redirect'] to see if we should redirect
		 * @uses VFH_User_Controls::$options['logout_redirect_to'] to see where we should redirect
		 * @uses get_permalink() to determine the link we're redirecting to
		 * @uses wp_safe_redirect() to handle the redirect
		 */
		function redirect_after_logout() {
			if ( true !== $this->options['logout_redirect'] ) {
				return;
			}
			$pid = absint( $this->options['logout_redirect_to'] );
			if ( ! empty( $pid ) ) {
				$p = get_post( $pid );
				if ( ! is_wp_error( $p ) && $p->ID == $pid ) {
					wp_safe_redirect( get_permalink( $pid ) );
					exit();
				}
			}
			wp_safe_redirect( home_url() );
			exit();
		}
		
		/**
		 * Hide the backend from specific user levels
		 * @uses VFH_User_Controls::$options['login_redirect_to'] to determine where the user should be redirected
		 * @uses VFH_User_Controls::$user_levels to see if the user needs to be redirected
		 */
		function hide_backend_from_user( $level='subscriber' ) {
			/**
			 * Attempt not to short-circuit AJAX requests
			 */
			if ( function_exists( 'doing_ajax' ) && doing_ajax() ) {
				return;
			}
			if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
				return;
			}
			
			$pid = $this->options['login_redirect_to'];
			$p = get_permalink( $pid );
			if ( empty( $pid ) || empty( $p ) ) {
				$p = home_url();
			}
			
			if ( ! is_user_logged_in() )
				return;
			
			if ( ! current_user_can( $this->user_levels[$level]['captotest'] ) ) {
				wp_safe_redirect( $p );
				exit();
			}
		}
		
		/**
		 * Hide the backend from specific user levels upon login
		 * @uses VFH_User_Controls::$options['hide_backend'] to see if we need to redirect
		 * @uses VFH_User_Controls::$user_levels to check the capability of the user
		 * @uses get_permalink() to retrieve the URL of the page we're redirecting to
		 */
		function login_redirect( $url, $alt_url, $user ) {
			if ( true !== $this->options['hide_backend'] ) {
				if ( empty( $url ) || $this->login_url( '' ) == $url )
					return admin_url();
					
				return $url;
			}
			
			if ( ! stristr( $url, admin_url() ) ) {
				return $url;
			}
			
			$level = $this->options['hide_backend_from'];
			
			if ( is_wp_error( $user ) )
				return $url;
			
			if ( true !== user_can( $user, $this->user_levels[$level]['captotest'] ) ) {
				$pid = $this->options['login_redirect_to'];
				$p = get_permalink( $pid );
				if ( empty( $pid ) || empty( $p ) ) {
					$p = home_url();
				}
				return $p;
			}
			
			if ( empty( $url ) || $this->login_url( '' ) == $url )
				return admin_url();
				
			return $url;
		}
		
		/**
		 * Output the login form in place of the shortcode
		 * @param array $atts the list of attributes allowed in this shortcode:
		 * 		lostpassword - determines whether the lost password link should be displayed with the form
		 * 		register - determines whether the registration link should be displayed with the form
		 * @return string the login form
		 */
		function login_shortcode( $atts=array() ) {
			if ( empty( $atts ) && ! is_array( $atts ) ) {
				$atts = array();
			}
			$rt = wp_login_form( array( 'echo' => false ) );
			if ( array_key_exists( 'lostpassword', $atts ) ) {
				$rt .= sprintf( '<p class="lost-password-link"><a href="%1$s" title="%2$s">%3$s</a></p>', wp_lostpassword_url(), apply_filters( 'vfh-login-lost-password-title', __( 'Request a new password', 'vfhlogin' ) ), apply_filters( 'vfh-login-lost-password-text', __( 'Lost your password?', 'vfhlogin' ) ) );
			}
			if ( array_key_exists( 'register', $atts ) ) {
				$rt .= sprintf( '<p class="register-link"><a href="%1$s" title="%2$s">%3$s</a></p>', wp_registration_url(), apply_filters( 'vfh-login-register-title', __( 'Register for this site', 'vfhlogin' ) ), apply_filters( 'vfh-login-register-text', __( 'Register', 'vfhlogin' ) ) );
			}
			return $rt;
		}
		
		/**
		 * Handle the logged_in shortcode
		 * Displays content to users that are logged in, but hides it from users that are not
		 * @param array $atts the list of attributes; not used in this shortcode
		 * @param string $content the content that should be displayed
		 * @return string a blank string for users that are not logged in; the appropriate content for users that are
		 * @uses do_shortcode() to allow use of other shortcodes inside of the content
		 */
		function logged_in_shortcode( $atts=array(), $content='' ) {
			if ( ! is_user_logged_in() ) {
				return '';
			}
			
			return do_shortcode( $content );
		}
		
		/**
		 * Handle the logged_out shortcode
		 * Displays content to users that are not logged in, but hides it from users that are
		 * @param array $atts the list of attributes; not used in this shortcode
		 * @param string $content the content that should be displayed
		 * @return string a blank string for users that are logged in; the appropriate content for users that are not
		 * @uses do_shortcode() to allow use of other shortcodes inside of the content
		 */
		function logged_out_shortcode( $atts=array(), $content='' ) {
			if ( is_user_logged_in() ) {
				return '';
			}
			
			return do_shortcode( $content );
		}
	}
}