# VFH Login #
**Contributors:** cgrymala

**Tags:** login, admin, multisite, password, register

**Requires at least:** 4.1

**Tested up to:** 4.3.1

**Stable tag:** 0.3.2

**License:** GPLv2 or later

**License URI:** http://www.gnu.org/licenses/gpl-2.0.html


Modifies the login, registration and password retrieval process on a multisite install to use URLs relative to the individual sites

## Description ##
This plugin modifies the way the login, registration and password retrieval processes work within an individual site in a multisite installation of WordPress.

This plugin was initially developed for the [Virginia Foundation for the Humanities](http://virginiahumanities.org/) [Festival of the Book](http://vabook.org/).

With this plugin, you have the following options in regards to the general user registration and login experience:

1. Set a custom subject & message for the password retrieval email message
1. Hide the admin bar from certain user roles
1. Hide the entire WordPress backend from certain user roles
1. Redirect users to a specific page on the site upon successful login
1. Create a completely custom login page, rather than using the default wp-login.php form
1. Create a completely custom registration page, rather than using the default
1. Redirect users to a specific page when they successfully logout of the site

This plugin also automatically filters all URL references within login, password retrieval, etc. messages that normally point to the root site/network URL & replaces them with references to the individual site URL.

### Shortcodes ###
This plugin also adds three new shortcodes that can be used anywhere on the site:

1. `[login_form]` - Outputs the login form
1. `[logged_in][/logged-in]` - Conditionally outputs the content found between the opening and closing tags only when the visitor is currently logged into the site
1. `[logged_out][/logged_out]` - Conditionally outputs the content found between the opening and closing tags only when the visitor is currently *not* logged into the site.

## Installation ##
1. Upload the `vfh-login` folder to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Visit Users -> Custom Login Settings to configure the plugin options

## Frequently Asked Questions ##
### How do I write a custom "Lost Password" message? ###
While you are modifying the options for the plugin, you can write a completely custom email message to be sent out to people that indicate they've lost their password. Within that message, you can use specific placeholders to output dynamic content:

1. `{username}` - Output the person's WordPress username
1. `{reset_url}` - Output the URL that's used to reset the user's password
1. `{site_name}` - Output the name of the individual site on which this plugin is being used
1. `{site_url}` - Output the URL to the individual site on which this plugin is being used

### Are there any placeholders I can use in the "Lost Password" email subject? ###
There are two placeholders you can also use in the subject for the "Lost Password" email. They are the `{site_name}` and `{site_url}` placeholders mentioned above.

### Can I create a custom registration form? ###
This plugin does not offer any utilities to actually generate the custom registration form. However, if you use another utility (such as Gravity Forms or any other plugin that allows you to set up custom user registration forms), you can use this plugin to force WordPress to point visitors to the page on which that custom registration form resides.

## Changelog ##
### 0.3.2 ###
* Fixes fatal error when not all admin settings are filled in
* Fixes PHP warning when loading admin settings page
* Adds feature to specify redirect_to in login link, which short-circuits the standard redirect setup within the plugin

### 0.3.1 ###
* Add readme information

### 0.3 ###
* Fixed some PHP warnings related to users not being logged in

### 0.1 ###
* Initial version
