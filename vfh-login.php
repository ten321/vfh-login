<?php
/**
 * Plugin Name: VFH Modified Login
 * Description: Modifies the login, registration and password retrieval process on a multisite install to use URLs relative to the individual sites
 * Version: 0.3.2
 * Author: cgrymala
 * Author URI: http://ten-321.com/
 * Text Domain: vfhlogin
 * License: GPL2
 */

if ( ! class_exists( 'VFH_User_Controls' ) ) {
	require_once( plugin_dir_path( __FILE__ ) . '/class-vfh-login.php' );
	global $vfh_user_controls_obj;
	$vhf_user_controls_obj = new VFH_User_Controls;
}